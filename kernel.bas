DECLARE SUB main()

#include once "io.bi"
#include once "vga.bi"

SUB multiboot ()
	ASM
	
		'setting up the multiboot header - see GRUB docs for details
		
		.set ALIGN,     1<<0
		.set MEMINFO,	1<<1
		.set FLAGS,		ALIGN | MEMINFO
		.set MAGIC,		0x1BADB002
		.set CHECKSUM,	-(MAGIC + FLAGS)
		
		.align 4
		.LONG MAGIC
		.LONG FLAGS
		.LONG CHECKSUM
		
		.set STACKSIZE, 0x4000
		.comm stack, STACKSIZE, 32
		
		.global loader
		
		loader:
			lea	 esp, stack + STACKSIZE
			push eax
			push ebx
			
			CALL MAIN
			
			cli
			hlt
	END Asm	
END SUB

SUB main ()
	CONST s = "Hello World\n"
	CONST st = "333"	
	terminal_initialize ()
	DIM i AS ULONG = 0
	while (i < 10)
		terminal_setcolor (vga_entry_color(i, VGA_COLOR_BLACK))
		terminal_writestring CPtr(Byte Ptr, @s)
		i += 1
	WEND
	i = 0
	while (i < 20)
		terminal_writestring CPtr(Byte Ptr, @st)
		i += 1
	WEND
END SUB

