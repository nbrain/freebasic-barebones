#ifndef __VGA_BI__
#define __VGA_BI__

Enum vga_color
	VGA_COLOR_BLACK = 0
	VGA_COLOR_BLUE = 1
	VGA_COLOR_GREEN = 2
	VGA_COLOR_CYAN = 3
	VGA_COLOR_RED = 4
	VGA_COLOR_MAGENTA = 5
	VGA_COLOR_BROWN = 6
	VGA_COLOR_LIGHT_GREY = 7
	VGA_COLOR_DARK_GREY = 8
	VGA_COLOR_LIGHT_BLUE = 9
	VGA_COLOR_LIGHT_GREEN = 10
	VGA_COLOR_LIGHT_CYAN = 11
	VGA_COLOR_LIGHT_RED = 12
	VGA_COLOR_LIGHT_MAGENTA = 13
	VGA_COLOR_LIGHT_BROWN = 14
	VGA_COLOR_WHITE = 15
END Enum

FUNCTION vga_entry_color (fg AS vga_color, bg AS vga_color) AS UBYTE
	vga_entry_color = fg Or bg Shl 4
END FUNCTION

FUNCTION vga_entry (uc AS Byte Ptr, con_color AS UBYTE) AS USHORT
	vga_entry = cast(USHORT, uc[0]) Or cast(USHORT, con_color) Shl 8 
END FUNCTION

FUNCTION strlen (src AS Byte Ptr) AS UINTEGER
	DIM length AS UINTEGER = 0
	While(src[length] <> 0)
		length += 1
	Wend
	strlen = length - 1
END FUNCTION

DIM SHARED VGA_WIDTH AS CONST UINTEGER = 80
DIM SHARED VGA_HEIGHT AS CONST UINTEGER = 25
DIM Shared terminal_row AS UINTEGER
DIM Shared terminal_col AS UINTEGER
DIM Shared terminal_color AS UBYTE
DIM Shared terminal_buffer AS USHORT Ptr
CONST NO_CHAR = "a"

SUB terminal_initialize ()
	terminal_row = 0
	terminal_col = 0
	terminal_color = vga_entry_color (VGA_COLOR_LIGHT_GREEN, VGA_COLOR_BLACK)
	terminal_buffer = CPtr(USHORT Ptr, &HB8000)
	For y AS UINTEGER = 0 To 25 STEP 1
		For x AS UINTEGER = 0 TO 80 STEP 1
			DIM index AS CONST UINTEGER = y * 80 + x
			terminal_buffer[index] = vga_entry (CPtr(Byte Ptr, @" "), terminal_color)
		NEXT x
	NEXT y
END SUB

SUB terminal_scroll ()
	For y AS UINTEGER = 0 To 25 Step 1
		For x AS UINTEGER = 0 To 80 Step 1
			DIM index AS CONST UINTEGER = y * 80 + x
			terminal_buffer[index] = cast(USHORT, terminal_buffer[((y+1)*80+x)])
		NEXT x
	NEXT y
	For y AS UINTEGER = 24 To 25 Step 1
		For x AS UINTEGER = 0 To 80 Step 1
			DIM index AS CONST UINTEGER = y * 80 +x
			terminal_buffer[index] = vga_entry (CPtr(Byte Ptr, @" "), terminal_color)
		NEXT x
	NEXT y
	terminal_row = 24
	terminal_col = 0
END SUB

SUB terminal_setcolor (con_color AS UBYTE)
	terminal_color = con_color
END SUB

SUB terminal_putentryat (c AS Byte Ptr, con_color AS UBYTE, x AS UINTEGER, y AS UINTEGER)
	DIM index AS Const UINTEGER = y * 80 + x
	terminal_buffer[index] = vga_entry (c, con_color)
END SUB

CONST NEWLINE = "\n"

FUNCTION terminal_putchar (c AS Byte Ptr) AS UINTEGER
	If (c[0] = CPtr(Byte Ptr, @NEWLINE)[0]) Then
		Select Case c[1]
			Case (cast(Byte Ptr, @NEWLINE)[1]) 
				terminal_col = 0
				terminal_row += 1
				terminal_putchar = 1 
			Case Else
				terminal_putchar = 1
		END Select
	Else
		terminal_putentryat c, terminal_color, terminal_col, terminal_row
		terminal_putchar = 0
	
		terminal_col += 1
	END If
	If (terminal_col = 80) Then
		terminal_col = 0
		terminal_row += 1
	END IF
	if (terminal_row = 25) Then
		terminal_scroll	
	END If
END FUNCTION 

SUB terminal_write (con_data AS Byte Ptr, size AS UINTEGER)
	FOR i AS UINTEGER = 0 TO size STEP 1
		i += terminal_putchar (CPtr(Byte Ptr, @con_data[i]))
	NEXT i
END SUB

SUB terminal_writestring (con_data AS Byte Ptr)
	terminal_write (con_data, strlen (con_data))
END SUB

#endif

