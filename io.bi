' io.bi
#ifndef __IO_BI__
#define __IO_BI__

SUB outb (port AS USHORT, value AS UBYTE)
	Asm
		push dx
		push ax
		mov dx, [port]
		mov ax, [value]
		out dx, ax
		pop dx
		pop ax
	END Asm
END SUB

Function inb (port AS USHORT) AS UBYTE
	Asm
		push dx
		push ax
		mov dx, [port]
		in ax, dx
		mov [inb], ax
		pop dx
		pop dx
	END Asm
END Function

SUB io_wait ()
	ASM
		push dx
		push ax
		mov dx, 0x80
		mov ax, 0
		out dx, ax
		pop dx
		pop ax
	END Asm
END SUB

#endif
